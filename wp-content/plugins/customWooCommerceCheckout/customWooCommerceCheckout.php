<?php
/**
 * Plugin Name: Custom WooCommerce Checkout Fields
 * Description: This plugin is used to add extra fields on checkout page
 **/

// Create extra fields for checkout page
function emersoft_custom_checkout_fields($fields){
    $fields['emersoft_extra_fields'] = array(
            'emersoft_dropdown' => array(
                'type' => 'select',
                'options' => array( 'first' => __( 'First Option' ), 'second' => __( 'Second Option' ), 'third' => __( 'Third Option' ) ),
                'required'      => true,
                'label' => __( 'Dropdown field' )
                )
            );

    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'emersoft_custom_checkout_fields' );

// Display extra fields on checkout page
function emersoft_extra_checkout_fields(){

    $checkout = WC()->checkout(); ?>

    <div class="extra-fields">
    <h3><?php _e( 'Additional Fields' ); ?></h3>

    <?php
       foreach ( $checkout->checkout_fields['emersoft_extra_fields'] as $key => $field ) : ?>

            <?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

        <?php endforeach; ?>
    </div>

<?php }
add_action( 'woocommerce_checkout_after_customer_details' ,'emersoft_extra_checkout_fields' );

// Save extra checkout fields on submit
function emersoft_save_extra_checkout_fields( $order_id, $posted ){
    // don't forget appropriate sanitization if you are using a different field type
    if( isset( $posted['emersoft_dropdown'] ) && in_array( $posted['emersoft_dropdown'], array( 'first', 'second', 'third' ) ) ) {
        update_post_meta( $order_id, '_emersoft_dropdown', $posted['emersoft_dropdown'] );
    }
}
add_action( 'woocommerce_checkout_update_order_meta', 'emersoft_save_extra_checkout_fields', 10, 2 );


// Display extra fields in admin dashboard
function emersoft_display_order_data_in_admin( $order ){  ?>
    <div class="order_data_column">

        <h4><?php _e( 'Additional Information', 'woocommerce' ); ?><a href="#" class="edit_address"><?php _e( 'Edit', 'woocommerce' ); ?></a></h4>
        <div class="address">
        <?php
            echo '<p><strong>' . __( 'Dropdown' ) . ':</strong>' . get_post_meta( $order->id, '_emersoft_dropdown', true ) . '</p>'; ?>
        </div>
        <div class="edit_address">
            <?php woocommerce_wp_text_input( array( 'id' => '_emersoft_dropdown', 'label' => __( 'Dropdown field' ), 'wrapper_class' => '_billing_company_field' ) ); ?>
        </div>
    </div>
<?php }
add_action( 'woocommerce_admin_order_data_after_order_details', 'emersoft_display_order_data_in_admin' );

// Save updated extra fields from admin dashboard
function emersoft_save_extra_details( $post_id, $post ){
    if( is_admin() ) {
        update_post_meta( $post_id, '_emersoft_dropdown', wc_clean( $_POST[ '_emersoft_dropdown' ] ) );
        $email_oc = new WC_Email_Customer_Completed_Order();
        $email_oc->trigger($post_id);
    }
}
add_action( 'woocommerce_process_shop_order_meta', 'emersoft_save_extra_details', 45, 2 );

// Email fields update 
function emersoft_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
    $fields['licence'] = array(
                'label' => __( 'Dropdown field' ),
                'value' => get_post_meta( $order->id, '_emersoft_dropdown', true ),
            );
    return $fields;
}
add_filter('woocommerce_email_order_meta_fields', 'emersoft_email_order_meta_fields', 10, 3 );
